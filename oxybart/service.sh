# debug
#exec 2>$MODPATH/debug.log
#set -x

until [ $(getprop sys.boot_completed). = 1. ]; do
  sleep 1
done

apps="com.oneplus.gallery com.oneplus.filemanager com.oneplus.deskclock com.oneplus.camera com.heytap.music com.oneplus.note net.oneplus.weather com.oneplus.soundrecorder"
permissions="android.permission.POST_NOTIFICATIONS android.permission.READ_EXTERNAL_STORAGE android.permission.READ_PHONE_STATE android.permission.READ_MEDIA_IMAGES android.permission.READ_MEDIA_AUDIO android.permission.READ_MEDIA_VIDEO android.permission.WRITE_EXTERNAL_STORAGE android.permission.ACCESS_MEDIA_LOCATION android.permission.ACCESS_NETWORK_STATE org.codeaurora.permission.POWER_OFF_ALARM android.permission.ACCESS_FINE_LOCATION android.permission.ACCESS_COARSE_LOCATION android.permission.CAMERA android.permission.RECORD_AUDIO android.permission.BLUETOOTH_CONNECT"

for app in $apps; do
  for permission in $permissions; do
    pm grant "$app" "$permission"
    echo "$(date): granting $app permission $permission"
  done
done

app_package="com.oneplus.camera"
check_interval=5  # in seconds
last_check_time=0
copy_file=false  # Flag to track if the file has been copied

# Function to check if a directory exists
check_directory() {
  if [ -d "$1" ]; then
    return 0  # Directory exists
  else
    return 1  # Directory does not exist
  fi
}

# Function to check if a file exists
check_file() {
  if [ -f "$1" ]; then
    return 0  # File exists
  else
    return 1  # File does not exist
  fi
}

while true; do
  current_time=$(date +%s)
  time_since_last_check=$((current_time - last_check_time))
  if [ "$time_since_last_check" -ge "$check_interval" ]; then
    if ! check_directory "/data/data/com.oneplus.camera/shared_prefs"; then
      echo "$(date): Directory does not exist, waiting..."
      sleep 5
      continue
    fi

    if [ "$copy_file" = false ]; then
      if ! check_file "/data/data/com.oneplus.camera/shared_prefs/features.override.xml"; then
        # File does not exist and has not been copied yet
        echo "$(date): File does not exist! Copying file..."
        cp "/data/adb/modules/oxybart/data/data/com.oneplus.camera/shared_prefs/features.override.xml" "/data/data/com.oneplus.camera/shared_prefs/features.override.xml"
        echo "$(date): File copied successfully!"
        appops set com.oneplus.camera RUN_ANY_IN_BACKGROUND ignore
        am force-stop "$app_package"
        copy_file=true  # Set the flag to indicate that the file has been copied
        break  # Break out of the loop since the file has been copied successfully
      else
        echo "$(date): File exist!"
        break
      fi
    fi

    last_check_time=$current_time
  else
    echo "$(date): Skipping app check, $check_interval seconds have not passed yet"
  fi
  sleep 5
done
